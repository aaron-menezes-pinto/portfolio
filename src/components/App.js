import React, {Component} from 'react';
import Projects from './Projects';
import SocialProfiles from './SocialProfiles';
import Title from './Title';
import profilePic from '../assets/profile.jpg'

class App extends Component{
    state = { displayBio: false };

    toggleDisplayBio = () => {
        this.setState({ displayBio: !this.state.displayBio });
    }
    render(){
        return (
            <div>
                <img src={ profilePic } alt='profile-pic' className='profile' />
                <h1>Hello!</h1>
                <p>My name is Aaron.</p>
                <Title />
                <p>I'm always looking to learn and work on meaningful projects.</p>
                {   this.state.displayBio ? (
                        <div>
                            <p>I live in Brooklyn, NY, and I grew up in the Bronx, NY</p>
                            <p>My favorite language is Python, and I code daily (since the age of 11).</p>
                            <p>
                                When I am not coding, I travel, read, play with my German Shepherd dog, 
                                or watch sports.
                            </p>
                            <button onClick={ this.toggleDisplayBio }>Show less</button>
                        </div>
                    ) : (
                        <div>
                            <button onClick={ this.toggleDisplayBio }>Read more</button>
                        </div>
                    )
                }
                <hr />
                <Projects />
                <hr />
                <SocialProfiles />
            </div>
        )
    }
}

export default App;
