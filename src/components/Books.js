import React from 'react';
import BOOKS from '../data/books';

const Book = ({ book: { author, title } }) => {
    return(
        <span><em>{ title }</em> - { author }</span>
    );
}

const Books = () => (
    <div>
        <h2>My Favorite Books:</h2>
        <ul style={{ listStyle: 'none' }}>
            {
                BOOKS.map( BOOK => {
                    return(
                        <li key={ BOOK.id }>
                            <Book key={ BOOK.id } book={ BOOK } />
                        </li>
                    );
                })
            }
        </ul>
    </div>
)

export default Books;
