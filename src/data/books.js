const BOOKS = [
    {
        id: 1,
        title: 'The Alchemist',
        author: 'Paul Coehlo'
    },
    {
        id: 2,
        title: 'The Happiness Advantage',
        author: 'Shawn Achor'
    },
    {
        id: 3,
        title: 'Man\'s Search for Meaning',
        author: 'Viktor Frankl'
    },
    {
        id: 4,
        title: 'The Great Gatsby',
        author: 'F. Scott Fitzgerald'
    },
    {
        id: 5,
        title: 'The Odyssey',
        author: 'Homer'
    },
    {
        id: 6,
        title: 'The Catcher in the Rye',
        author: 'J. D. Salinger'
    },
    {
        id: 7,
        title: 'To Kill a Mockingbird',
        author: 'Harper Lee'
    },
    {
        id: 8,
        title: 'The Lord of the Rings',
        author: 'J. R. R. Tolkien'
    },
    {
        id: 9,
        title: 'The Call of the Wild',
        author: 'Jack London'
    },
    {
        id: 10,
        title: 'Switch: How to Change Things When Change is Hard',
        author: 'Dan Heath & Chip Heath'
    }
];

export default BOOKS;
