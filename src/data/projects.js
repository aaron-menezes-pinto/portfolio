import project1 from '../assets/project1.png';
import project2 from '../assets/project2.png';
import project3 from '../assets/project3.png';

const PROJECTS = [
    {   
        id: 1,
        title: 'Music Master',
        description: 'A React App that I built while taking an online course!',
        link: 'https://aaron-menezes-pinto.gitlab.io/music-master',
        image: project1
    },
    {
        id: 2,
        title: 'My Portfolio',
        description: 'My Personal Portfolio source code!',
        link: 'https://gitlab.com/aaron-menezes-pinto/portfolio',
        image: project2
    },
    {
        id: 3,
        title: 'Business App',
        description: 'A Business App I built!',
        link: 'https://aaron-menezes-pinto.gitlab.io/business-app',
        image: project3
    }
];

export default PROJECTS;
