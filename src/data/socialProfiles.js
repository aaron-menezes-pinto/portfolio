import emailIcon from '../assets/email_icon.png';
import gitlabIcon from '../assets/gitlab_icon.svg';
import linkedinIcon from '../assets/linkedin_icon.png';
import instagramIcon from '../assets/instagram_icon.png';
import twitterIcon from '../assets/twitter_icon.png';

const SOCIAL_PROFILES = [
    {
        id: 1,
        link: 'mailto:amenezes13@gmail.com',
        image: emailIcon
    },
    {
        id: 2,
        link: 'https://gitlab.com/aaron-menezes-pinto/portfolio',
        image: gitlabIcon
    },
    {
        id: 3,
        link: 'https://www.linkedin.com/in/aaron-menezes-pinto-72a51160',
        image: linkedinIcon
    },
    {
        id: 4,
        link: 'https://www.instagram.com/badguysays/',
        image: instagramIcon
    },
	{
        id: 5,
        link: 'https://www.twitter.com/badguysays/',
        image: twitterIcon
    }
];

export default SOCIAL_PROFILES;