import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Header from './components/Header';
import App from './components/App';
import Jokes from './components/Jokes';
import Books from './components/Books';
import './index.css';

ReactDOM.render(
    <BrowserRouter basename={ process.env.PUBLIC_URL }>
        <Switch>
            <Route exact path='/' render={ () => <Header><App /></Header> } />
            <Route path='/jokes' render={ () => <Header><Jokes /></Header> } />
            <Route path='/books' render={ () => <Header><Books /></Header> } />
        </Switch>
    </BrowserRouter>, 
    document.getElementById('root')
);
